package controlefornecedor.model;

public class PessoaJuridica extends Fornecedor {
    private String cnpj;
    private String razaoSocial;

    
    public PessoaJuridica(){
    
    }
    public PessoaJuridica(String nome, String telefoneFixo, String telefoneMovel, 
            String cnpj){
        
        super(nome, telefoneFixo, telefoneMovel);
        this.cnpj = cnpj;
    
    
    }
    /**
     * @return the cpnj
     */
    public String getCnpj() {
        return cnpj;
    }

    /**
     * @param cpnj the cpnj to set
     */
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    /**
     * @return the razãoSocial
     */
    public String getRazaoSocial() {
        return razaoSocial;
    }

    /**
     * @param razãoSocial the razãoSocial to set
     */
    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
    
}
