package controlefornecedor.model;

public class PessoaFisica extends Fornecedor {
    private String cpf;

    public PessoaFisica(){
    
    }
    
    public PessoaFisica(String nome, String telefoneFixo, String telefoneMovel, 
            String cpf){
        
        super(nome, telefoneFixo, telefoneMovel);
        this.cpf = cpf;
        
    }
    /**
     * @return the cpf
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * @param cpf the cpf to set
     */
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
}
