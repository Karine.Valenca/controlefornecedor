package controlefornecedor.model;

public class Produto {
    private String nomeProduto;
    private String descricao;
    private String valorCompra;
    private String valorVenda;
    private String quantidadeEstoque;
    
    public Produto(){
    
    }

    public Produto(String nomeProduto) {
        
    }

    /**
     * @return the nome
     */
    public String getNomeProduto() {
        return nomeProduto;
    }

    /**
     * @param nome the nome to set
     */
    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the valorCompra
     */
    public String getValorCompra() {
        return valorCompra;
    }

    /**
     * @param valorCompra the valorCompra to set
     */
    public void setValorCompra(String valorCompra) {
        this.valorCompra = valorCompra;
    }

    /**
     * @return the valorVenda
     */
    public String getValorVenda() {
        return valorVenda;
    }

    /**
     * @param valorVenda the valorVenda to set
     */
    public void setValorVenda(String valorVenda) {
        this.valorVenda = valorVenda;
    }

    /**
     * @return the quantidadeEstoque
     */
    public String getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    /**
     * @param quantidadeEstoque the quantidadeEstoque to set
     */
    public void setQuantidadeEstoque(String quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }
      
}
