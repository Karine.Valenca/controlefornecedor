package controlefornecedor.controller;

import controlefornecedor.model.Fornecedor;
import java.util.ArrayList;

public class ControleFornecedor {
    private ArrayList<Fornecedor> listaFornecedores;
    
      
    
    public ControleFornecedor(){
        
        listaFornecedores = new ArrayList<Fornecedor>();
    
    }
    
    
    public void adicionar(Fornecedor umFornecedor){
        listaFornecedores.add(umFornecedor);
        
    }
    
    public Fornecedor pesquisar(String nome){
        for(Fornecedor umFornecedor: listaFornecedores){
            if(umFornecedor.getNome().equalsIgnoreCase(nome))
                return umFornecedor;
            }
        return null;
        }
    public void remover(Fornecedor umFornecedor){
        listaFornecedores.remove(umFornecedor);
    }
}
