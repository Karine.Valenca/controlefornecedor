package controlefornecedor.controller;

import controlefornecedor.model.PessoaFisica;
import java.util.ArrayList;

public class ControlePessoaFisica {
    private ArrayList<PessoaFisica> listaPessoaFisica;
    
    public ControlePessoaFisica(){ 
        listaPessoaFisica = new ArrayList<PessoaFisica>();
    }
    
    public void adicionarPessoaFisica(PessoaFisica umaPessoaFisica){
        listaPessoaFisica.add(umaPessoaFisica);
    }
    
    public void removerPessoaFisica(PessoaFisica umaPessoaFisica){
        listaPessoaFisica.remove(umaPessoaFisica);
    }
    
}
    
