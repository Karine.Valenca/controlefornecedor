package controlefornecedor.controller;

import controlefornecedor.model.PessoaJuridica;
import java.util.ArrayList;

public class ControlePessoaJuridica {
    private ArrayList<PessoaJuridica> listaPessoaJuridica;
    
    public ControlePessoaJuridica(){
        listaPessoaJuridica = new ArrayList<PessoaJuridica>();
    }
    
    public void adicionarPessoaJuridica(PessoaJuridica umaPessoaJuridica){
        listaPessoaJuridica.add(umaPessoaJuridica);
    }
    
    public void removerPessoaJuridica(PessoaJuridica umaPessoaJuridica){
        listaPessoaJuridica.remove(umaPessoaJuridica);
    }
}
