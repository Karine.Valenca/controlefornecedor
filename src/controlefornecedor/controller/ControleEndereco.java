
package controlefornecedor.controller;

import controlefornecedor.model.Endereco;
import java.util.ArrayList;


public class ControleEndereco {
    private ArrayList<Endereco> listaEnderecos;
    
    
    public ControleEndereco(){
        listaEnderecos = new ArrayList<Endereco>();    
    }
    
    public void adicionarEndereco(Endereco umEndereco){
        listaEnderecos.add(umEndereco);
    }
}
