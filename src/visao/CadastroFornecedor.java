package visao;

import controlefornecedor.controller.ControleFornecedor;
import controlefornecedor.controller.ControlePessoaFisica;
import controlefornecedor.controller.ControlePessoaJuridica;
import controlefornecedor.controller.ControleEndereco;
import controlefornecedor.controller.ControleProduto;
import controlefornecedor.model.Fornecedor;
import controlefornecedor.model.PessoaFisica;
import controlefornecedor.model.PessoaJuridica;
import controlefornecedor.model.Produto;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import controlefornecedor.model.Endereco;


public class CadastroFornecedor extends javax.swing.JFrame {
    Fornecedor umFornecedor = new Fornecedor();
    PessoaFisica umaPessoaFisica = new PessoaFisica();
    PessoaJuridica umaPessoaJuridica = new PessoaJuridica();
    ControleFornecedor umControleFornecedor = new ControleFornecedor();
    ControleEndereco umControleEndereco = new ControleEndereco();
    ControlePessoaFisica umControlePessoaFisica = new ControlePessoaFisica();
    ControlePessoaJuridica umControlePessoaJuridica = new ControlePessoaJuridica();
    Endereco umEndereco = new Endereco();
    Produto umProduto = new Produto();
    ControleProduto umControleProduto = new ControleProduto();
            
    private final byte PESSOAFISICA = 0;
    private final byte PESSOAJURIDICA = 1;
    
    
    public CadastroFornecedor() {
        initComponents();
        limparTela();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabelTipo = new javax.swing.JLabel();
        jComboBoxTipoFornecedor = new javax.swing.JComboBox();
        jLabelNome = new javax.swing.JLabel();
        jTextFieldNome = new javax.swing.JTextField();
        jLabelTelefoneFixo = new javax.swing.JLabel();
        jTextFieldTelefoneFixo = new javax.swing.JTextField();
        jLabelEndereco = new javax.swing.JLabel();
        jLabelCpf = new javax.swing.JLabel();
        jTextFieldCpf = new javax.swing.JTextField();
        jLabelCnpj = new javax.swing.JLabel();
        jTextFieldCnpj = new javax.swing.JTextField();
        jLabelRazaoSocial = new javax.swing.JLabel();
        jTextFieldRazaoSocial = new javax.swing.JTextField();
        jButtonSalvar = new javax.swing.JButton();
        jButtonPesquisar = new javax.swing.JButton();
        jButtonExcluir = new javax.swing.JButton();
        jLabelEstado = new javax.swing.JLabel();
        jLabelCIdade = new javax.swing.JLabel();
        jLabelBairro = new javax.swing.JLabel();
        jLabelCep = new javax.swing.JLabel();
        jLabelLogradouro = new javax.swing.JLabel();
        jLabelNumero = new javax.swing.JLabel();
        jLabelComplemento = new javax.swing.JLabel();
        jLabelTitulo = new javax.swing.JLabel();
        jTextFieldNumero = new javax.swing.JTextField();
        jTextFieldComplemento = new javax.swing.JTextField();
        jTextFieldLogradouro = new javax.swing.JTextField();
        jTextFieldCep = new javax.swing.JTextField();
        jTextFieldBairro = new javax.swing.JTextField();
        jTextFieldCidade = new javax.swing.JTextField();
        jTextFieldEstado = new javax.swing.JTextField();
        jLabelProduto = new javax.swing.JLabel();
        jLabelNomeProduto = new javax.swing.JLabel();
        jTextFieldNomeProduto = new javax.swing.JTextField();
        jLabelDescricaoProduto = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextAreaDescricaoProduto = new javax.swing.JTextArea();
        jLabelValorCompra = new javax.swing.JLabel();
        jTextFieldValorCompra = new javax.swing.JTextField();
        jLabelValorVenda = new javax.swing.JLabel();
        jTextFieldValorVenda = new javax.swing.JTextField();
        jLabelQuantidade = new javax.swing.JLabel();
        jTextFieldQuantidade = new javax.swing.JTextField();
        jLabelTelefoneMovel = new javax.swing.JLabel();
        jTextFieldTelefoneMovel = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setToolTipText("");

        jLabelTipo.setText("Tipo de fornecedor:");

        jComboBoxTipoFornecedor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Pessoa Física", "Pessoa Jurídica" }));
        jComboBoxTipoFornecedor.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxTipoFornecedorItemStateChanged(evt);
            }
        });

        jLabelNome.setText("Nome:");

        jTextFieldNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldNomeActionPerformed(evt);
            }
        });

        jLabelTelefoneFixo.setText("Tel. Fixo:");

        jTextFieldTelefoneFixo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTelefoneFixoActionPerformed(evt);
            }
        });

        jLabelEndereco.setText("Endereço:");

        jLabelCpf.setText("CPF:");

        jTextFieldCpf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCpfActionPerformed(evt);
            }
        });

        jLabelCnpj.setText("CNPJ:");

        jTextFieldCnpj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldCnpjActionPerformed(evt);
            }
        });

        jLabelRazaoSocial.setText("Razão Social:");

        jTextFieldRazaoSocial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldRazaoSocialActionPerformed(evt);
            }
        });

        jButtonSalvar.setText("Salvar");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jButtonPesquisar.setText("Pesquisar");
        jButtonPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisarActionPerformed(evt);
            }
        });

        jButtonExcluir.setText("Excluir");
        jButtonExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonExcluirActionPerformed(evt);
            }
        });

        jLabelEstado.setText("Estado:");

        jLabelCIdade.setText("Cidade:");

        jLabelBairro.setText("Bairro:");

        jLabelCep.setText("CEP:");

        jLabelLogradouro.setText("Logradouro:");

        jLabelNumero.setText("Número:");

        jLabelComplemento.setText("Complemento:");

        jLabelTitulo.setText("Cadastro Fornecedor");

        jTextFieldComplemento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldComplementoActionPerformed(evt);
            }
        });

        jLabelProduto.setText("Produto");

        jLabelNomeProduto.setText("Nome:");

        jLabelDescricaoProduto.setText("Descrição:");

        jTextAreaDescricaoProduto.setColumns(20);
        jTextAreaDescricaoProduto.setRows(5);
        jScrollPane1.setViewportView(jTextAreaDescricaoProduto);

        jLabelValorCompra.setText("Valor Compra:");

        jLabelValorVenda.setText("Valor Venda:");

        jLabelQuantidade.setText("Quantidade em estoque:");

        jLabelTelefoneMovel.setText("Tel. Móvel:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(271, 271, 271)
                        .addComponent(jLabelTitulo))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabelTipo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBoxTipoFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(304, 304, 304)
                                .addComponent(jLabelProduto))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(173, 173, 173)
                                        .addComponent(jLabelEndereco))
                                    .addComponent(jLabelCep)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabelComplemento)
                                            .addComponent(jLabelNumero)
                                            .addComponent(jLabelLogradouro)
                                            .addComponent(jLabelBairro)
                                            .addComponent(jLabelCIdade)
                                            .addComponent(jLabelEstado))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                                            .addComponent(jTextFieldNumero)
                                            .addComponent(jTextFieldLogradouro)
                                            .addComponent(jTextFieldCep)
                                            .addComponent(jTextFieldBairro)
                                            .addComponent(jTextFieldCidade)
                                            .addComponent(jTextFieldEstado)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(jButtonSalvar)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonPesquisar)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonExcluir))))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabelTelefoneFixo)
                                            .addComponent(jLabelNome))
                                        .addGap(45, 45, 45)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jTextFieldNome)
                                            .addComponent(jTextFieldTelefoneFixo, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                                            .addComponent(jTextFieldTelefoneMovel)
                                            .addComponent(jTextFieldCpf)
                                            .addComponent(jTextFieldCnpj, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                                            .addComponent(jTextFieldRazaoSocial)))
                                    .addComponent(jLabelTelefoneMovel)
                                    .addComponent(jLabelRazaoSocial)
                                    .addComponent(jLabelCnpj)
                                    .addComponent(jLabelCpf))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 142, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabelQuantidade)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabelValorCompra)
                                            .addComponent(jLabelValorVenda))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jTextFieldValorVenda)
                                            .addComponent(jTextFieldValorCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabelDescricaoProduto)
                                            .addComponent(jLabelNomeProduto))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))))
                .addContainerGap(33, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelTitulo)
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxTipoFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelTipo)
                    .addComponent(jLabelProduto))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNome)
                            .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelTelefoneFixo)
                            .addComponent(jTextFieldTelefoneFixo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelTelefoneMovel)
                            .addComponent(jTextFieldTelefoneMovel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCpf)
                            .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCnpj)
                            .addComponent(jTextFieldCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(17, 17, 17)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelRazaoSocial)
                            .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addComponent(jLabelEndereco)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelEstado)
                            .addComponent(jTextFieldEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCIdade)
                            .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelBairro)
                            .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelCep)
                            .addComponent(jTextFieldCep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelLogradouro)
                            .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNumero)
                            .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelComplemento)
                            .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButtonSalvar)
                            .addComponent(jButtonPesquisar)
                            .addComponent(jButtonExcluir)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelNomeProduto)
                            .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelValorCompra)
                                    .addComponent(jTextFieldValorCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelValorVenda)
                                    .addComponent(jTextFieldValorVenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabelQuantidade)
                                    .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabelDescricaoProduto)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(141, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void preencherCampos(){
    this.jTextFieldNome.setText(umFornecedor.getNome()); 
    this.jTextFieldTelefoneFixo.setText(umFornecedor.getTelefoneFixo());
    this.jTextFieldTelefoneMovel.setText(umFornecedor.getTelefoneMovel());
    this.jTextFieldCpf.setText(umFornecedor.getCpf());
    this.jTextFieldCnpj.setText(umFornecedor.getCnpj());
    this.jTextFieldRazaoSocial.setText(umFornecedor.getRazaoSocial());
    this.jTextFieldEstado.setText(umEndereco.getEstado());
    this.jTextFieldCidade.setText(umEndereco.getCidade());
    this.jTextFieldBairro.setText(umEndereco.getBairro());
    this.jTextFieldCep.setText(umEndereco.getCep());
    this.jTextFieldLogradouro.setText(umEndereco.getLogradouro());
    this.jTextFieldNumero.setText(umEndereco.getNumero());
    this.jTextFieldComplemento.setText(umEndereco.getComplemento());
    this.jTextFieldNomeProduto.setText(umProduto.getNomeProduto());
    this.jTextAreaDescricaoProduto.setText(umProduto.getDescricao());
    this.jTextFieldValorCompra.setText(umProduto.getValorCompra());
    this.jTextFieldValorVenda.setText(umProduto.getValorVenda());
    this.jTextFieldQuantidade.setText(umProduto.getQuantidadeEstoque());
    }
    
    private void limparTela(){
    this.jTextFieldNome.setText("");
    this.jTextFieldTelefoneFixo.setText("");  
    this.jTextFieldTelefoneMovel.setText("");
    this.jTextFieldCpf.setText("");
    this.jTextFieldCnpj.setText("");
    this.jTextFieldRazaoSocial.setText("");
    this.jTextFieldEstado.setText("");
    this.jTextFieldCidade.setText("");
    this.jTextFieldBairro.setText("");
    this.jTextFieldCep.setText("");
    this.jTextFieldLogradouro.setText("");
    this.jTextFieldNumero.setText("");
    this.jTextFieldComplemento.setText("");
    this.jTextFieldNomeProduto.setText("");
    this.jTextAreaDescricaoProduto.setText("");
    this.jTextFieldValorVenda.setText("");
    this.jTextFieldValorCompra.setText("");
    this.jTextFieldQuantidade.setText("");
    }
    
    private void pesquisar(String nome){
        Fornecedor fornecedorPesquisado = umControleFornecedor.pesquisar(nome);
            if(fornecedorPesquisado == null){
                JOptionPane.showMessageDialog(rootPane, "Pessoa não encontrada");
            }else{
                this.umFornecedor = fornecedorPesquisado;
                this.preencherCampos();
                }
    
    }

    private PessoaFisica salvarPessoaFisica(){
        String nome = this.jTextFieldNome.getText();
        String telefoneFixo = this.jTextFieldTelefoneFixo.getText();
        String telefoneMovel = this.jTextFieldTelefoneMovel.getText();
        String cpf = this.jTextFieldCpf.getText();
        umaPessoaFisica = new PessoaFisica(nome, telefoneFixo, telefoneMovel, 
                cpf);
        
        umFornecedor.setNome(jTextFieldNome.getText());
        umFornecedor.setTelefoneFixo(jTextFieldTelefoneFixo.getText());
        umFornecedor.setTelefoneMovel(jTextFieldTelefoneMovel.getText());
        umFornecedor.setCpf(jTextFieldCpf.getText());
        
        umControleFornecedor.adicionar(umaPessoaFisica);
        return umaPessoaFisica;
    }
    
    private PessoaJuridica salvarPessoaJuridica(){
        String nome = this.jTextFieldNome.getText();
        String telefoneFixo = this.jTextFieldTelefoneFixo.getText();
        String telefoneMovel = this.jTextFieldTelefoneMovel.getText();
        String cnpj = this.jTextFieldCnpj.getText();
        
        umaPessoaJuridica = new PessoaJuridica(nome, telefoneFixo, telefoneMovel,
                cnpj);
        
        umFornecedor.setNome(jTextFieldNome.getText());
        umFornecedor.setTelefoneFixo(jTextFieldTelefoneFixo.getText());
        umFornecedor.setTelefoneMovel(jTextFieldTelefoneMovel.getText());
        umFornecedor.setCnpj(jTextFieldCnpj.getText());
        umFornecedor.setRazaoSocial(jTextFieldRazaoSocial.getText());
        
        umControleFornecedor.adicionar(umaPessoaJuridica);
        return umaPessoaJuridica;
    }
    
    private Endereco salvarEndereco(){
        String cep = this.jTextFieldCep.getText();
        umEndereco = new Endereco(cep);
        
        umEndereco.setEstado(jTextFieldEstado.getText());
        umEndereco.setCidade(jTextFieldCidade.getText());
        umEndereco.setBairro(jTextFieldBairro.getText());
        umEndereco.setCep(jTextFieldCep.getText());
        umEndereco.setLogradouro(jTextFieldLogradouro.getText());
        umEndereco.setNumero(jTextFieldNumero.getText());
        umEndereco.setComplemento(jTextFieldComplemento.getText());
        
        umControleEndereco.adicionarEndereco(umEndereco);
        return umEndereco;
    }
    
    private Produto salvarProduto(){
        String nomeProduto = this.jTextFieldNomeProduto.getText();
        umProduto = new Produto(nomeProduto);
        
        umProduto.setNomeProduto(jTextFieldNomeProduto.getText());
        umProduto.setDescricao(jTextAreaDescricaoProduto.getText());
        umProduto.setValorCompra(jTextFieldValorCompra.getText());
        umProduto.setValorVenda(jTextFieldValorVenda.getText());
        umProduto.setQuantidadeEstoque(jTextFieldQuantidade.getText());
        
        umControleProduto.adicionarProduto(umProduto);
        return umProduto;
    }
    
private void jTextFieldNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldNomeActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jTextFieldNomeActionPerformed

private void jTextFieldTelefoneFixoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTelefoneFixoActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jTextFieldTelefoneFixoActionPerformed

private void jTextFieldCpfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCpfActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jTextFieldCpfActionPerformed

private void jTextFieldCnpjActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldCnpjActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jTextFieldCnpjActionPerformed

private void jTextFieldRazaoSocialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldRazaoSocialActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jTextFieldRazaoSocialActionPerformed

private void jComboBoxTipoFornecedorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxTipoFornecedorItemStateChanged
    if(this.jComboBoxTipoFornecedor.getSelectedItem().toString().equals("Pessoa Física")){
        this.jTextFieldCpf.setEnabled(true);
        this.jTextFieldCnpj.setEnabled(false);
        this.jTextFieldRazaoSocial.setEnabled(false);    
    }else{
        this.jTextFieldCpf.setEnabled(false);
        this.jTextFieldCnpj.setEnabled(true);
        this.jTextFieldRazaoSocial.setEnabled(true);
    }
}//GEN-LAST:event_jComboBoxTipoFornecedorItemStateChanged

private void jButtonExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonExcluirActionPerformed
    umControleFornecedor.remover(umFornecedor);
    limparTela();
}//GEN-LAST:event_jButtonExcluirActionPerformed

private void jButtonPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarActionPerformed
    String pesquisa = JOptionPane.showInputDialog("Digite o nome do Contato.");
        if (pesquisa != null) {
            pesquisar(pesquisa);
        }
}//GEN-LAST:event_jButtonPesquisarActionPerformed

private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed
    switch(jComboBoxTipoFornecedor.getSelectedIndex()){
        case PESSOAFISICA:
            
            umaPessoaFisica = this.salvarPessoaFisica();
        
        case PESSOAJURIDICA:
            
            umaPessoaJuridica = this.salvarPessoaJuridica();
            
    }
    
    salvarEndereco();
    salvarProduto();
    umControleFornecedor.adicionar(umFornecedor);
    umControleEndereco.adicionarEndereco(umEndereco);
    umControleProduto.adicionarProduto(umProduto);
    
    String mensagem = "Contato adicionado com sucesso";
    JOptionPane.showMessageDialog(null, mensagem);
    
    limparTela();
}//GEN-LAST:event_jButtonSalvarActionPerformed

private void jTextFieldComplementoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldComplementoActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_jTextFieldComplementoActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new CadastroFornecedor().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonExcluir;
    private javax.swing.JButton jButtonPesquisar;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JComboBox jComboBoxTipoFornecedor;
    private javax.swing.JLabel jLabelBairro;
    private javax.swing.JLabel jLabelCIdade;
    private javax.swing.JLabel jLabelCep;
    private javax.swing.JLabel jLabelCnpj;
    private javax.swing.JLabel jLabelComplemento;
    private javax.swing.JLabel jLabelCpf;
    private javax.swing.JLabel jLabelDescricaoProduto;
    private javax.swing.JLabel jLabelEndereco;
    private javax.swing.JLabel jLabelEstado;
    private javax.swing.JLabel jLabelLogradouro;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JLabel jLabelNomeProduto;
    private javax.swing.JLabel jLabelNumero;
    private javax.swing.JLabel jLabelProduto;
    private javax.swing.JLabel jLabelQuantidade;
    private javax.swing.JLabel jLabelRazaoSocial;
    private javax.swing.JLabel jLabelTelefoneFixo;
    private javax.swing.JLabel jLabelTelefoneMovel;
    private javax.swing.JLabel jLabelTipo;
    private javax.swing.JLabel jLabelTitulo;
    private javax.swing.JLabel jLabelValorCompra;
    private javax.swing.JLabel jLabelValorVenda;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextAreaDescricaoProduto;
    private javax.swing.JTextField jTextFieldBairro;
    private javax.swing.JTextField jTextFieldCep;
    private javax.swing.JTextField jTextFieldCidade;
    private javax.swing.JTextField jTextFieldCnpj;
    private javax.swing.JTextField jTextFieldComplemento;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldEstado;
    private javax.swing.JTextField jTextFieldLogradouro;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldNomeProduto;
    private javax.swing.JTextField jTextFieldNumero;
    private javax.swing.JTextField jTextFieldQuantidade;
    private javax.swing.JTextField jTextFieldRazaoSocial;
    private javax.swing.JTextField jTextFieldTelefoneFixo;
    private javax.swing.JTextField jTextFieldTelefoneMovel;
    private javax.swing.JTextField jTextFieldValorCompra;
    private javax.swing.JTextField jTextFieldValorVenda;
    // End of variables declaration//GEN-END:variables
    
    
}
